# SIMPLE DOTFILES

Contains dotfiles for GNU/Linux

Clone repository to ~/dotfiles and run install script.
CAUTION!! Existing dotfiles will be DELETED!

    git clone https://gitlab.com/Qvav3/dotfiles.git ~/dotfiles && bash ~/dotfiles/install


## Configuration

The ~/.bashrc file can contain local settings.
Specify the variable $PCNAME before sourcing ~/.bashrc2 to change the name
in the shell prompt.


## License

Released under the MIT License. See [LICENSE.md](LICENSE.md) for details.
